import os
import bs4
import time
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from PyPDF2 import PdfFileMerger
import urllib

main_url = "http://gujaratsamacharepaper.com/download.php?file=http://enewspapr.com/News/GUJARAT/SUR/"

if time.localtime(time.time()).tm_mon<10 :
	month = '0'+str(time.localtime(time.time()).tm_mon)
else :
	month = time.localtime(time.time()).tm_mon;

if time.localtime(time.time()).tm_mday<10 :
	date = '0'+str(time.localtime(time.time()).tm_mday)
else :
	date = time.localtime(time.time()).tm_mday;

pdfs = []
id = '2018'+str(month)+str(date)+'_'

merger = PdfFileMerger()


for i in range(1, 35):
        back_url = '2018' + '/'+str(month)+'/'+str(date)+'/'+str(id)+str(i)+'.PDF'
        print(main_url+back_url)
        respose = urllib.request.urlopen(main_url+str(back_url))
        pagename = 'gssurp'+str(i)+'.pdf'
        pdfs.append(pagename)
        fl = open(pagename, 'wb')
        fl.write(respose.read())
        fl.close()
        if os.stat('gssurp'+str(i)+'.pdf').st_size > 0:
            merger.append('gssurp'+str(i)+'.pdf')

for i in range(1, 35):
    os.system('rm '+'gssurp'+str(i)+'.pdf'+' 2>>dump')

try:
    merger.write("gssur.pdf")
    os.system('python changepdf.py gssur 2>>dump')
    os.system('python sendpdf.py gssur 2>>dump')
except:
    os.system('python senderr.py gssur 2>>dump')
